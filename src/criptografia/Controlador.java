package criptografia;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import arquivo.Arquivo;
import input.Mensageiro;

public class Controlador {
	public static void main(String[] args) {
		int action = Mensageiro.getAction();
		File file = Mensageiro.chooseFilePath();
		String key = Mensageiro.getKey();
		Cifrador cifrador = new Cifrador();
		File newFile;
		byte[] data;
		
		if(file == null) {
			System.out.println("Caminho nulo");
			return;
		}
		if(key == null) {
			System.out.println("Chave nula");
			return;
		}
//		Recebe os bytes do arquivo
		try {
			data = Arquivo.getBytes(file.toString());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		try {
			cifrador.setCifrador();
			cifrador.setKey(key);
			
			if(action == 0) {//encrypt
				data = cifrador.encrypt(data);
				newFile = new File(file.getParentFile() + "/encriptado." + Arquivo.getExtension(file));
				Arquivo.writeBytes(newFile.toString(), data);
			}else if(action == 1) {//decrypt
				data = cifrador.decrypt(data);
				newFile = new File(file.getParentFile() + "/decriptado." + Arquivo.getExtension(file));
				Arquivo.writeBytes(newFile.toString(), data);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return;
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return;
		} catch (InvalidKeyException e) {
			Mensageiro.wrongKey();
			return;
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			return;
		} catch (BadPaddingException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
