package criptografia;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Cifrador {
	private final String NOME_ALGORITMO = "Blowfish";
	private SecretKeySpec secretKey;
	private Cipher cipher;

	public void setCifrador() throws NoSuchAlgorithmException, NoSuchPaddingException {
		 this.cipher = Cipher.getInstance(NOME_ALGORITMO); // Retorna o cifrador do algoritmo especificado.
	 }
	
	public void setKey(String key) throws NoSuchAlgorithmException {
		secretKey = new SecretKeySpec(key.getBytes(), NOME_ALGORITMO);// Gera a chave.
	}
	
	public byte[] encrypt(byte[] data) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		byte[] encriptedData = null;
		this.cipher.init(Cipher.ENCRYPT_MODE, secretKey); // Indica a chave e estabelece o modo cifrador.
		encriptedData = this.cipher.doFinal(data); // Criptografa a cadeia de bytes
		return encriptedData; // Retorna a cadeia criptografada
	}
	
	public byte[] decrypt(byte[] data) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		byte[] decriptedData = null;
		this.cipher.init(Cipher.DECRYPT_MODE, secretKey); // Indica a chave e estabelece o modo decifrador.
		decriptedData = this.cipher.doFinal(data); // Decriptografa a cadeia de bytes
		return decriptedData; // Retorna a cadeia decriptografada
	}
}
