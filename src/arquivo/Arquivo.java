package arquivo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Arquivo {
	public static byte[] getBytes(String filePath) throws IOException {
		Path path = Paths.get(filePath);
		byte[] bytes = Files.readAllBytes(path);
		return bytes;
	}

	public static void writeBytes(String filePath, byte[] bytes) throws IOException {
		Path path = Paths.get(filePath);
		Files.write(path, bytes);
	}
	
	public static String getExtension(File file) {
		String ext = "";
		String [] fileName = file.getName().split("\\.");
		ext = fileName[fileName.length - 1];
		if(ext.equals(file.getName()))
			return "";
		
		return ext;
	}
}
