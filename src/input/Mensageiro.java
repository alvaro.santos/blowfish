package input;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class Mensageiro {

	public static String getKey() {
		String message = "Senha:";
		return JOptionPane.showInputDialog(null, message);
	}

	public static String getConfirmationKey() {
		String message = "Confirmar senha:";
		return JOptionPane.showInputDialog(null, message);
	}

	public static int getAction() {
		String message = "Qual a��o?";
		String[] options = { "Encriptar", "Decriptar" };
		int choose = JOptionPane.showOptionDialog(null, message, null, JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options,
				null);
		return choose;
	}

	public static File chooseFilePath(){
		File file= null;
		JFileChooser chooser = new JFileChooser();
		if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			file = chooser.getSelectedFile();
		}
		return file;
	}
	
	public static String chooseSavePath(){
		String path = null;
		JFileChooser chooser = new JFileChooser();
		if(chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			path = chooser.getSelectedFile().toString();
		}
		return path;
	}
	
	public static void notSameKey() {
		String message = "As chaves s�o diferentes";
		JOptionPane.showMessageDialog(null, message);
	}
	
	public static void wrongKey() {
		String message = "As chave est� incorreta!";
		JOptionPane.showMessageDialog(null, message);
	}
}
